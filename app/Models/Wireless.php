<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Wireless extends Model
{

	public $timestamps = false;

	protected $table = 'wireless';
	protected $primaryKey = 'id';

	protected $fillable = [
		'id',
		'user_id',
		'mdn',
		'esn',
	];

	// Relation with User:
	public function User() {
		return $this->hasOne('App\User', 'id', 'user_id');
	}

}