# laravel-nova-demo

## Getting Started

### 1. Laravel Nova requires php bcmath extension. Install it:
```
# sudo add-apt-repository ppa: ondrej / php
# sudo apt update
# sudo apt install php7.2-bcmath
```

### 2. Create a database and customize it in an .env file:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laranova
DB_USERNAME=username
DB_PASSWORD=password
```

### 3. Unpack the package with Laravel Nova (in our case from the repository) into the folder with the project:
```
# git clone git@gitlab.com: pixwireless / laravel-nova-sandbox.git
```

### 4. Than run:
```
# composer update
# php artisan nova:install
# php artisan migrate
```

### 5. Create an admin account:
```
# php artisan tinker
>>> $user = new App\User;
>>> $user->name="Admin";
>>> $user->email="laranova-admin@cimpleo.com";
>>> $user->password=bcrypt('password');
>>> $user->save();

```

### Done! Laravel Nova is available at:
```
http://[YOUR SITE]/nova
```
If desired, the path can be changed in the project file config / nova.php)